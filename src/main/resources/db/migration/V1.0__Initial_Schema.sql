create table AppUser (
  user_id bigserial primary key,
  name text not null,
  username varchar(500) unique not null,
  email text,
  password text not null,
  active boolean not null default true
);

create table Location (
  location_id bigserial primary key,
  user_id bigint not null references AppUser,
  name text not null
);

create table Book (
  book_id bigserial primary key,
  isbn char(13) not null,
  title text not null,
  author text
);

create table Book_Location (
  book_location_id bigserial primary key,
  book_id bigint not null references Book,
  location_id bigint not null references Location,
  amount int not null default(1)
);

create table Borrower (
  borrower_id bigserial primary key,
  location_id bigint not null references Location,
  name text not null
);

create table Borrowed (
  borrowed_id bigserial primary key,
  borrower_id bigint not null references Borrower,
  book_location_id bigint not null references Book_Location,
  borrowed_date timestamptz not null default(now()),
  returned_date timestamptz
);

