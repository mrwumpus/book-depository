alter table AppUser add
    active_location_id bigint references Location(location_id);

update AppUser set active_location_id = location_id
    from Location where Location.user_id=AppUser.user_id;
