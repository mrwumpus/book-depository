alter table Book_Location
    add constraint book_id_location_id
    unique (book_id, location_id);