alter table Book
    add constraint book_isbn
    unique (isbn);