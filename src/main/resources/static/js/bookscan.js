$(function() {
    let $scanArea = $('#scanArea')
    let $scanButton = $('#startScan')

    function closeArea() {
        Quagga.stop()
        $scanArea.toggleClass('d-none')
        $scanButton.prop('disabled', false)
    }

    Quagga.onDetected(function(data) {
        closeArea()
        let code = data.codeResult.code
        $('#isbn').val(code)
    })

    $('#stopButton').on('click', function(evt) {
            evt.preventDefault()
            closeArea()
        }
    )

    $scanButton.on('click', function(evt) {
            evt.preventDefault()
            $scanButton.prop('disabled', true)
            Quagga.init({
                inputStream: {
                    name: 'Live',
                    type: 'LiveStream',
                    size: 1024
                },
                decoder: {
                    readers: ['ean_reader']
                },
                locator: {
                    patchSize: 'medium',
                }
             }, function(err) {
                    if(!err) {
                        $scanArea.toggleClass('d-none')
                        Quagga.start()
                    } else {
                        $scanButton.prop('disabled', false)
                    }
                }
            )
        }
    )

    $scanButton.prop('disabled', false)
})
