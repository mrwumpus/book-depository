package net.paksao.bookdepository.location;

import net.paksao.bookdepository.domain.AppUser;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;

@Mapper
public interface LocationMapper {

    @Insert("insert into location ( user_id, name ) values ( #{userId}, #{name} )")
    @Options(keyColumn = "location_id", useGeneratedKeys = true)
    void createDefaultLocation(final Location location);

}
