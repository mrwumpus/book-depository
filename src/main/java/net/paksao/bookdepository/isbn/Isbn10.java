package net.paksao.bookdepository.isbn;

public class Isbn10 {
    private final String code;

    private Isbn10(final String code) {
        this.code = code;
    }

    public static Isbn10 code(final String code) {
        if(code != null) {
            final String cleaned = code.replaceAll("-", "").replaceAll(" ", "");
            if (isValid(cleaned)) {
                return new Isbn10(cleaned);
            }
        }

        return null;
    }

    private static boolean isValid(final String code) {
        if(code.length() != 10) { return false; }
        if(!code.codePoints().allMatch(Character::isDigit)) {
            return false;
        }

        int sum = 0;
        int accum = 0;
        for(int i = 0; i < code.length(); ++i) {
            final int digit = Character.digit(code.codePointAt(i), 10);
            accum += digit;
            sum += accum;
        }

        return sum % 11 == 0;
    }

    public String code() {
        return this.code;
    }

    @Override
    public String toString() {
        return "Isbn10{" +
                "code='" + code + '\'' +
                '}';
    }
}
