package net.paksao.bookdepository.isbn;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = IsbnValidator.class)
public @interface IsbnConstraint {
    String message() default "Not a valid ISBN";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
