package net.paksao.bookdepository.isbn;

public class Isbn13 {

    private final String code;

    private Isbn13(final String code) {
        this.code = code;
    }

    public static Isbn13 code(final String code) {
        if (code != null) {
            final String cleaned = code.replaceAll("-", "").replaceAll(" ", "");
            if (isValid(cleaned)) {
                return new Isbn13(cleaned);
            }
        }

        return null;
    }

    private static boolean isValid(final String code) {
        if (code.length() != 13 ||
                !code.codePoints().allMatch(Character::isDigit)) {
            return false;
        }

        int sum = 0;
        for (int i = 0; i < code.length(); ++i) {
            final int multiplier = i % 2 == 0 ? 1 : 3;
            final int digit = Character.digit(code.codePointAt(i), 10);
            sum += digit * multiplier;
        }

        return sum % 10 == 0;
    }

    public String code() {
        return this.code;
    }

    @Override
    public String toString() {
        return "Isbn13{" +
                "code='" + code + '\'' +
                '}';
    }
}
