package net.paksao.bookdepository.isbn;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class IsbnValidator implements ConstraintValidator<IsbnConstraint, String> {
    @Override
    public void initialize(final IsbnConstraint constraintAnnotation) {
    }

    @Override
    public boolean isValid(final String isbn, final ConstraintValidatorContext constraintValidatorContext) {
        if(isbn == null) {
            return false;
        }

        if(isbn.length() == 10) {
            return Isbn10.code(isbn) != null;
        }

        if(isbn.length() == 13) {
            return Isbn13.code(isbn) != null;
        }

        return false;
    }
}
