package net.paksao.bookdepository.domain;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class Book {

    private Long id;
    @NotEmpty
    @Size(min = 13, max = 13, message = "must be ISBN-13")
    private String isbn;
    private String title;
    private String author;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
