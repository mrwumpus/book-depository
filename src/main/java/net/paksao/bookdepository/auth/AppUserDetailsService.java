package net.paksao.bookdepository.auth;

import net.paksao.bookdepository.appUser.AppUserMapper;
import net.paksao.bookdepository.domain.AppUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

public class AppUserDetailsService implements UserDetailsService {

    private static final Logger logger = LoggerFactory.getLogger(AppUserDetailsService.class);

    @Autowired
    private AppUserMapper appUserMapper;

    @Autowired
    private PasswordEncoder pwEncoder;

    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        final AppUser appUser = appUserMapper.findByUsername(username);
        if(appUser == null) {
            throw new UsernameNotFoundException("Bad username or password");
        }

        return new AppUserDetails(appUser);
    }
}
