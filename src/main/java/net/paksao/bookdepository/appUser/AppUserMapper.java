package net.paksao.bookdepository.appUser;

import net.paksao.bookdepository.domain.AppUser;
import net.paksao.bookdepository.location.Location;
import org.apache.ibatis.annotations.*;

@Mapper
public interface AppUserMapper {

    @Select("select count(*) from AppUser")
    int appUserCount();

    @Select("select * from AppUser where username = #{username}")
    @Results({@Result(column = "user_id", property = "id")})
    AppUser findByUsername(@Param("username") final String username);

    @Insert("insert into AppUser ( username, name, password ) values ( #{username}, #{name}, #{password})")
    @Options(keyColumn = "user_id", useGeneratedKeys = true)
    void registerUser(final AppUser appUser);

    @Update("update AppUser set active_location_id = location_id from Location where Location.user_id = AppUser.user_id " +
            "and Location.location_id = #{location.id} and AppUser.user_id = #{appUser.id} and AppUser.username = #{username}")
    void setActiveLocation(final AppUser appUser, final Location location, final String username);

}
