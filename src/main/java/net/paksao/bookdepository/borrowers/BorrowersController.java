package net.paksao.bookdepository.borrowers;

import net.paksao.bookdepository.book.BookMapper;
import net.paksao.bookdepository.exception.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.security.Principal;

@Controller
@RequestMapping("/borrowers")
public class BorrowersController {

    private static final Logger log = LoggerFactory.getLogger(BorrowersController.class);

    @Autowired
    private BorrowerMapper borrowerMapper;

    @Autowired
    private BookMapper bookMapper;

    @GetMapping
    String getBorrowers(final Borrower borrower, final Model model, final Principal principal) {
        model.addAttribute("borrowers", borrowerMapper.getAppUserBorrowers(principal.getName()));
        return "borrowers";
    }

    @PostMapping("/{id}")
    String saveOrUpdateBorrower(@PathVariable final long id, @Valid final Borrower borrower, final BindingResult bindingResult,
                                final Model model, final Principal principal) {
        if (id > 0) {
            final Borrower current = borrowerMapper.getBorrower(id, principal.getName());

            if (current == null) {
                throw new UnauthorizedException();
            }

            model.addAttribute("current", current);
        }

        if (bindingResult.hasErrors()) {
            return id > 0 ? "borrowerForm" : getBorrowers(borrower, model, principal);
        }

        if (id == 0) {
            borrowerMapper.saveBorrower(borrower, principal.getName());
        } else {
            borrowerMapper.updateBorrower(borrower, principal.getName());
        }

        return "redirect:/borrowers";
    }

    @PostMapping(params = "delete", path = "/{id}")
    String deleteBorrower(@PathVariable final long id, final Principal principal) {
        if(borrowerMapper.deleteBorrowedIfAllReturned(id, principal.getName()) == 0) {
            throw new UnauthorizedException();
        }
        if(borrowerMapper.deleteBorrower(id, principal.getName()) == 0) {
            throw new UnauthorizedException();
        }
        return "redirect:/borrowers";
    }

    @GetMapping("/{id}")
    String editBorrower(@PathVariable final long id, final Principal principal, final Model model) {
        Borrower borrower = new Borrower();

        if(id > 0) {
            borrower = borrowerMapper.getBorrower(id, principal.getName());
        }

        if(borrower == null) {
            throw new UnauthorizedException();
        }

        model.addAttribute("current", borrower);
        model.addAttribute(borrower);
        return "borrowerForm";
    }

    @GetMapping("/{id}/borrowed")
    String getBorrowed(@PathVariable final long id, final Model model, final Principal principal) {
        final Borrower borrower = borrowerMapper.getBorrower(id, principal.getName());
        if(borrower == null) {
            throw new UnauthorizedException();
        }

        model.addAttribute("borrower", borrower);
        model.addAttribute("borrowed", borrowerMapper.getBorrowedByBorrowerId(id, principal.getName()));
        return "borrowed";
    }

    @PostMapping("/{id}/return")
    String returnBorrowed(@PathVariable final long id,
                          final long borrowedId,
                          final Principal principal) {
        var borrower = borrowerMapper.getBorrower(id, principal.getName());
        if(borrower == null) {
            throw new UnauthorizedException();
        }
        borrowerMapper.returnBorrowed(borrowedId, principal.getName());
        return "redirect:/borrowers/{id}/borrowed";
    }

    @GetMapping("/{id}/borrow")
    String borrowList(@PathVariable("id") final long id, final Model model, final Principal principal) {
        final Borrower borrower = borrowerMapper.getBorrower(id, principal.getName());
        if(borrower == null) {
            throw new UnauthorizedException();
        }
        model.addAttribute("borrower", borrower);
        model.addAttribute("books", bookMapper.getBooksToBorrow(principal.getName()));
        return "borrow";
    }

    @PostMapping("/{id}/borrow")
    String borrow(@PathVariable("id") final long id, final Long bookId, final Principal principal) {
        if(bookMapper.borrowBook(bookId, id, principal.getName()) == 0) {
            throw new UnauthorizedException();
        }
        return "redirect:/borrowers/{id}/borrowed";
    }
}
