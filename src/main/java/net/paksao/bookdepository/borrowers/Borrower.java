package net.paksao.bookdepository.borrowers;

import javax.validation.constraints.NotEmpty;

public class Borrower {

    private long id = 0;
    @NotEmpty
    private String name;
    private int bookCount = 0;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBookCount() {
        return bookCount;
    }

    public void setBookCount(int bookCount) {
        this.bookCount = bookCount;
    }
}
