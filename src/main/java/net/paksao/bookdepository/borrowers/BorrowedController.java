package net.paksao.bookdepository.borrowers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
@RequestMapping("/borrowed")
public class BorrowedController {

    @Autowired
    private BorrowerMapper borrowerMapper;

    @GetMapping
    String getBorrowed(final Model model, final Principal principal) {
        model.addAttribute("borrowed", borrowerMapper.getBorrowed(principal.getName()));
        return "borrowed";
    }
}
