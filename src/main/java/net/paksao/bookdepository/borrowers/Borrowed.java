package net.paksao.bookdepository.borrowers;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class Borrowed {

    private Long id;

    private String title;
    private String isbn;
    private String borrower;
    private int borrowerId;
    private ZonedDateTime borrowedDate;
    private ZonedDateTime returnedDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getBorrower() {
        return borrower;
    }

    public void setBorrower(String borrower) {
        this.borrower = borrower;
    }

    public int getBorrowerId() {
        return borrowerId;
    }

    public void setBorrowerId(int borrowerId) {
        this.borrowerId = borrowerId;
    }

    public ZonedDateTime getBorrowedDate() {
        return borrowedDate;
    }

    private String dateToString(final ZonedDateTime zonedDateTime) {
        final var nyZone = ZoneId.of("America/New_York");
        final var convert = zonedDateTime.withZoneSameInstant(nyZone);
        return DateTimeFormatter.ofPattern("yyyy-MM-dd 'at' hh:mm a").format(convert);
    }

    public String getBorrowedDateString() {
        return dateToString(this.getBorrowedDate());
    }

    public void setBorrowedDate(ZonedDateTime borrowedDate) {
        this.borrowedDate = borrowedDate;
    }

    public ZonedDateTime getReturnedDate() {
        return returnedDate;
    }

    public String getReturnedDateString() {
        return dateToString(this.getReturnedDate());
    }

    public void setReturnedDate(ZonedDateTime returnedDate) {
        this.returnedDate = returnedDate;
    }

}
