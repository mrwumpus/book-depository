package net.paksao.bookdepository.borrowers;

import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface BorrowerMapper {

    @Select("select b.*, borrowed.count bookCount from Borrower b " +
            "join Location using (location_id) join appuser using (user_id) " +
            "left join (select borrower_id, count(book_location_id) from Borrowed " +
            "where returned_date is null group by borrower_id) borrowed using ( borrower_id ) " +
            "where appuser.username = #{username} and appuser.active_location_id = location_id")
    @Results({@Result(column = "borrower_id", property = "id")})
    List<Borrower> getAppUserBorrowers(final String username);

    @Select("select b.*, borrowed.count bookCount from Borrower b " +
            "join Location using (location_id) join appuser using (user_id) " +
            "left join (select borrower_id, count(book_location_id) from Borrowed " +
            "where returned_date is null group by borrower_id) borrowed using ( borrower_id ) " +
            "where appuser.username = #{username} and b.borrower_id = #{borrowerId}")
    @Results({@Result(column = "borrower_id", property = "id")})
    Borrower getBorrower(final long borrowerId, final String username);

    @Insert("insert into Borrower ( name, location_id ) select #{borrower.name}, location_id from Location " +
            "join AppUser using (user_id) where AppUser.username = #{username} and AppUser.active_location_id = location_id")
    void saveBorrower(final Borrower borrower, @Param("username") final String username);

    @Update("update Borrower set name = #{borrower.name} where borrower_id = (select distinct borrower_id from Borrower " +
            "join Location using (location_id) join AppUser using (user_id) " +
            "where AppUser.username = #{username} and Borrower.borrower_id = #{borrower.id})")
    int updateBorrower(final Borrower borrower, @Param("username") final String username);

    @Delete("delete from Borrower where borrower_id = (select distinct borrower_id from Borrower " +
            "join Location using (location_id) join AppUser using (user_id) left join Borrowed using (borrower_id) " +
            "where borrower_id = #{borrowerId} and AppUser.username = #{username} and Borrowed.borrowed_id is null)")
    int deleteBorrower(final long borrowerId, final String username);

    @Delete("delete from Borrowed b using Borrower join Location using (location_id) " +
            "join AppUser using (user_id) where b.borrower_id = #{borrowerId} and " +
            "Borrower.borrower_id = b.borrower_id and AppUser.username = #{username} " +
            "and NOT EXISTS (select true from Borrowed where borrower_id = #{borrowerId} " +
            "and Borrowed.returned_date is null)")
    int deleteBorrowedIfAllReturned(final long borrowerId, final String username);

    @Select("select borrowed_id, borrowed_date borrowedDate, returned_date returnedDate, " +
            "Borrower.name borrower, isbn, title from Borrowed " +
            "join Book_Location using (book_location_id) join Location using (location_id) " +
            "join Book using (book_id) join Borrower using (borrower_id) join AppUser using (user_id) " +
            "where Borrower.borrower_id = #{borrowerId} and AppUser.username = #{username}")
    @Results({@Result(column = "borrowed_id", property = "id")})
    List<Borrowed> getBorrowedByBorrowerId(final long borrowerId, final String username);

    @Select("select borrowed_id, borrowed_date borrowedDate, returned_date returnedDate, " +
            "Borrower.borrower_id borrowerId, Borrower.name borrower, isbn, title from Borrowed " +
            "join Book_Location using (book_location_id) join Location using (location_id) " +
            "join Book using (book_id) join Borrower using (borrower_id) join AppUser using (user_id) " +
            "where AppUser.username = #{username} and returned_date is null " +
            "and AppUser.active_location_id = Location.location_id")
    @Results({@Result(column = "borrowed_id", property = "id")})
    List<Borrowed> getBorrowed(final String username);

    @Update("update Borrowed set returned_date=now() where borrowed_id = " +
            "(select distinct borrowed_id from Borrowed join Book_Location using (book_location_id) " +
            "join Location using (location_id) join AppUser using (user_id) " +
            "where AppUser.username = #{username} and borrowed_id = #{borrowedId} and returned_date is null)")
    int returnBorrowed(final long borrowedId, final String username);

}
