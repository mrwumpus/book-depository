package net.paksao.bookdepository.book;

import net.paksao.bookdepository.domain.Book;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface BookMapper {

    @Select("select b.*, bl.book_location_id bookLocationId, bl.amount total, blc.checkedOut from Book b " +
            "join Book_Location bl using (book_id) " +
            "join Location using (location_id) join AppUser using (user_id) " +
            "left join (select book_location_id, count(borrowed_id) checkedOut from Borrowed " +
            "where returned_date is null " +
            "group by book_location_id) blc using (book_location_id)" +
            "where AppUser.username = #{username} and AppUser.active_location_id = location_id")
    @Results({@Result(column = "book_id", property = "bookId")})
    List<BookData> getBooks(@Param("username") final String username);

    @Select("select b.*, bl.book_location_id bookLocationId, bl.amount total, blc.checkedOut from Book b " +
            "join Book_Location bl using (book_id) " +
            "join Location using (location_id) join AppUser using (user_id) " +
            "left join (select book_location_id, count(borrowed_id) checkedOut from Borrowed " +
            "where returned_date is null " +
            "group by book_location_id) blc using (book_location_id)" +
            "where AppUser.username = #{username} and book_location_id = #{bookLocationId} and AppUser.active_location_id = location_id")
    @Results({@Result(column = "book_id", property = "bookId")})
    BookData getBookData(final long bookLocationId, final String username);

    @Select("select b.*, bl.book_location_id bookLocationId, bl.amount total, 0 checkedOut from Book b " +
            "join Book_Location bl using (book_id) " +
            "join Location using (location_id) join AppUser using (user_id) " +
            "where AppUser.username = #{username} and b.book_id = #{bookId} and AppUser.active_location_id = location_id")
    @Results({@Result(column = "book_id", property = "bookId")})
    BookData getBookDataForBook(final long bookId, final String username);

    @Select("select b.*, bl.book_location_id bookLocationId, bl.amount total, blc.checkedOut from Book b join Book_Location bl using (book_id) " +
            "join Location using (location_id) join AppUser using (user_id) " +
            "left join (select book_location_id, count(borrowed_id) checkedOut from Borrowed where returned_date is null " +
            "group by book_location_id) blc using (book_location_id)" +
            "where AppUser.username = #{username} and bl.amount > 0 " +
            "and AppUser.active_location_id = location_id " +
            "and (blc.checkedOut is null or bl.amount > blc.checkedOut)")
    @Results({@Result(column = "book_id", property = "bookId")})
    List<BookData> getBooksToBorrow(final String username);

    @Select("select b.*, bl.book_location_id bookLocationId, bl.amount total, blc.checkedOut from Book b join Book_Location bl using (book_id) " +
            "join Location using (location_id) join AppUser using (user_id) " +
            "left join (select book_location_id, count(borrowed_id) checkedOut from Borrowed where returned_date is null " +
            "group by book_location_id) blc using (book_location_id) " +
            "left join Borrowed on Borrowed.borrower_id=#{borrowerId} and Borrowed.book_location_id=bl.book_location_id " +
            "where AppUser.username = #{username} and bl.amount > 0 " +
            "and AppUser.active_location_id = location_id " +
            "and (blc.checkedOut is null or bl.amount > blc.checkedOut) and Borrowed.borrowed_id is null")
    @Results({@Result(column = "book_id", property = "bookId")})
    List<BookData> getBooksToBorrowForBorrowerId(final long borrowerId, final String username);

    @Select("select * from Book where isbn=#{isbn}")
    @Results({@Result(column = "book_id", property = "id")})
    Book getBookByIsbn(final String isbn);

    @Insert("insert into Book ( isbn, title, author ) values ( #{isbn}, #{title}, #{author} )")
    @Options(keyColumn = "book_id", useGeneratedKeys = true)
    void addBook(final Book book);

    @Update("update Book set isbn = #{isbn}, title = #{title}, author = #{author} where book_id = #{id}")
    void updateBook(final Book book);

    @Insert("insert into Book_Location (book_id, location_id, amount) " +
            "select #{bookId}, loc.location_id, #{amount} from AppUser join Location loc using (user_id) " +
            "where AppUser.username = #{username} and AppUser.active_location_id = location_id " +
            "on conflict (book_id, location_id) do update " +
            "set amount = Book_Location.amount + EXCLUDED.amount")
    void addBookToActiveLocation(final long bookId, final int amount, final String username);

    @Update("update Book_Location set book_id = #{bookId}, amount = #{amount} where book_location_id in " +
            "(select book_location_id from Book_location join Location using (location_id) join AppUser using (user_id) " +
            "where book_location_id = #{bookLocationId} and AppUser.username = #{username})")
    int updateBookLocation(final long bookLocationId, final long bookId, final int amount, final String username);

    @Delete("delete from Book_Location where book_location_id in " +
            "(select book_location_id from Book_Location join Location using (location_id) join AppUser using (user_id)" +
            "where book_location_id = #{bookLocationId} and AppUser.username = #{username})")
    int deleteBookLocation(final long bookLocationId, final String username);

    @Insert("insert into Borrowed (borrower_id, book_location_id) " +
            "select borrower_id, book_location_id from AppUser join Location using (user_id) " +
            "join Book_location using (location_id) join Borrower using (location_id) " +
            "left join (select book_location_id, count(borrowed_id) checkedOut from Borrowed where returned_date is null " +
            "group by book_location_id) blc using (book_location_id)" +
            "where book_location_id = #{bookLocationId} and borrower_id = #{borrowerId} and " +
            "amount > 0 and (blc.checkedOut is null or amount > blc.checkedOut) and AppUser.username = #{username}")
    int borrowBook(final long bookLocationId, final long borrowerId, final String username);
}
