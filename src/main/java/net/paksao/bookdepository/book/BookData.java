package net.paksao.bookdepository.book;

public class BookData {
    private Long bookId;
    private Long bookLocationId;
    private String isbn;
    private String title;
    private String author;
    private int total = 0;
    private int checkedOut = 0;

    public Long getBookId() {
        return bookId;
    }

    public Long getBookLocationId() {
        return bookLocationId;
    }

    public String getIsbn() {
        return isbn;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public int getTotal() {
        return total;
    }

    public int getCheckedOut() {
        return checkedOut;
    }
}
