package net.paksao.bookdepository.book;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.paksao.bookdepository.domain.Book;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;

public class OpenLibraryIsbnSearch {

    private static final String BASE_URL = "https://openlibrary.org/api/books?jscmd=data&format=json&bibkeys=ISBN:";

    private OpenLibraryIsbnSearch() {
        // private constructor
    }

    private static URL searchUrl(final String isbn) throws MalformedURLException {
        Objects.nonNull(isbn);
        return new URL(OpenLibraryIsbnSearch.BASE_URL + isbn);
    }

    public static Book searchIsbn(final String isbn) throws IOException {
        final ObjectMapper mapper = new ObjectMapper();
        final JsonNode json = mapper.readTree(searchUrl(isbn));
        final JsonNode isbnNode = json.get("ISBN:" + isbn);

        if(isbnNode != null) {
            final Book book = new Book();
            book.setIsbn(isbn);
            book.setTitle(isbnNode.get("title").asText());
            book.setAuthor(isbnNode.path("authors").iterator().next().get("name").asText());

            return book;
        }

        return null;
    }
}
