package net.paksao.bookdepository.book;

import net.paksao.bookdepository.isbn.IsbnConstraint;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class BookForm {
    private long id = 0;
    @IsbnConstraint
    private String isbn;
    @NotEmpty
    private String title;
    private String author;
    @Min(1)
    @NotNull
    private Integer amount = Integer.valueOf(1);

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}
