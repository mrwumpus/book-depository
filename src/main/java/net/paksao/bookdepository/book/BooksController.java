package net.paksao.bookdepository.book;

import net.paksao.bookdepository.domain.Book;
import net.paksao.bookdepository.exception.UnauthorizedException;
import net.paksao.bookdepository.isbn.Isbn10;
import net.paksao.bookdepository.isbn.Isbn13;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/books")
public class BooksController {

    static final Logger log = LoggerFactory.getLogger(BooksController.class);

    @Autowired
    private BookMapper bookMapper;

    @GetMapping
    public String books(final Model model, final Principal principal) {
        final List<BookData> books = bookMapper.getBooks(principal.getName());
        model.addAttribute("books", books);
        return "books";
    }

    @GetMapping("/{id}")
    public String editBook(@PathVariable final int id, final Model model, final Principal principal) {
        final BookForm bookForm = new BookForm();
        if (id > 0) {
            final BookData bookData = bookMapper.getBookData(id, principal.getName());
            if (bookData != null) {
                bookForm.setIsbn(bookData.getIsbn());
                bookForm.setAuthor(bookData.getAuthor());
                bookForm.setTitle(bookData.getTitle());
                bookForm.setAmount(bookData.getTotal());
                bookForm.setId(bookData.getBookLocationId());
            } else {
                throw new UnauthorizedException();
            }
        }
        model.addAttribute(bookForm);

        return "bookForm";
    }

    @PostMapping(params = "delete", path = "/{id}")
    String deleteBookLocation(@PathVariable final long id, final Principal principal) {
        return "redirect:/books";
    }

    @PostMapping(params = "search", path = "/{id}")
    public String searchBook(final BookForm bookForm, final Model model, final BindingResult binding) {
        bookForm.setTitle("");
        bookForm.setAuthor("");

        String cleanIsbn;
        final Isbn13 isbn13 = Isbn13.code(bookForm.getIsbn());
        if (isbn13 == null) {
            final Isbn10 isbn10 = Isbn10.code(bookForm.getIsbn());
            if (isbn10 == null) {
                binding.rejectValue("isbn", "isbn.bad", "Invalid ISBN, please enter another number.");
                return "bookForm";
            }

            cleanIsbn = isbn10.code();
        } else {
            cleanIsbn = isbn13.code();
        }


        bookForm.setIsbn(cleanIsbn);

        Book book = bookMapper.getBookByIsbn(cleanIsbn);
        if (book == null) {
            try {
                book = OpenLibraryIsbnSearch.searchIsbn(cleanIsbn);
            } catch (IOException ex) {
                book = null;
            }
        }
        if (book != null) {
            bookForm.setAuthor(book.getAuthor());
            bookForm.setTitle(book.getTitle());
        } else {
            model.addAttribute("message", "Unable to locate book for the given ISBN; please enter it manually.");
        }
        return "bookForm";

    }

    private boolean sameAuthorTitle(final BookForm bookForm, final Book existing) {
        return existing != null && existing.getTitle().equals(bookForm.getTitle()) && existing.getAuthor().equals(bookForm.getAuthor());
    }

    private Book createBook(final String cleanIsbn, final BookForm bookForm) {
        final Book newBook = new Book();
        newBook.setIsbn(cleanIsbn);
        newBook.setAuthor(bookForm.getAuthor().trim());
        newBook.setTitle(bookForm.getTitle().trim());
        // create the book record.
        bookMapper.addBook(newBook);
        return newBook;
    }

    @PostMapping("/{id}")
    @Transactional
    public String saveEditBook(@PathVariable final int id, @Valid final BookForm bookForm,
                               final BindingResult bindingResult, final Model model, final Principal principal) {
        if (bindingResult.hasErrors()) {
            return "bookForm";
        }

        String cleanIsbn;
        if (bookForm.getIsbn().length() == 10) {
            cleanIsbn = Isbn10.code(bookForm.getIsbn()).code();
        } else {
            cleanIsbn = Isbn13.code(bookForm.getIsbn()).code();
        }

        Book existingBook = bookMapper.getBookByIsbn(cleanIsbn);

        if (id > 0) {
            BookData bookData = bookMapper.getBookData(id, principal.getName());

            if(bookData == null) {
                throw new UnauthorizedException();
            }

            if (bookData.getCheckedOut() > bookForm.getAmount()) {
                bindingResult.rejectValue("amount", "amount.fewerThanCheckedOut",
                        String.format("Cannot reduce available below the number of checked out: %d", bookData.getCheckedOut()));
                return "bookForm";
            }

            if (bookData.getIsbn().equals(cleanIsbn)) {
                if (!sameAuthorTitle(bookForm, existingBook)) {
                    existingBook.setTitle(bookForm.getTitle());
                    existingBook.setAuthor(bookForm.getAuthor());
                    bookMapper.updateBook(existingBook);
                }
            } else {
                // The ISBN has changed
                if (bookData.getCheckedOut() > 0) { // Can't change ISBN when checked out.
                    bindingResult.rejectValue("isbn", "isbn.checkedOut",
                            String.format("ISBN cannot be updated when checked out. Current ISBN is checked out to %d borrowers.",
                                    bookData.getCheckedOut()));
                    return "bookForm";
                }
                if (existingBook != null && !sameAuthorTitle(bookForm, existingBook)) {
                    model.addAttribute("errorMessage", "ISBN is already in use. Please select another or click the search button to load the existing author and title.");
                    return "bookForm";
                }

                if(existingBook == null) {
                    existingBook = createBook(cleanIsbn, bookForm);
                } else {
                    // Find existing book location for the existing book.
                    final BookData existingData = bookMapper.getBookDataForBook(existingBook.getId(), principal.getName());
                    // if found, update the existing count and delete the old book location.
                    if(existingData != null) {
                        final int grandTotal = existingData.getTotal() + bookForm.getAmount();
                        bookMapper.deleteBookLocation(id, principal.getName());
                        bookMapper.updateBookLocation(existingData.getBookLocationId(), existingData.getBookId(),
                                grandTotal, principal.getName());

                        return "redirect:/books";
                    }
                }
            }

            bookMapper.updateBookLocation(id, existingBook.getId(), bookForm.getAmount(), principal.getName());
        } else {

            if (existingBook == null) {
                existingBook = createBook(cleanIsbn, bookForm);
            } else if(!sameAuthorTitle(bookForm, existingBook)) {
                model.addAttribute("errorMessage", "ISBN is already in use. Please select another or click the search button to load the existing author and title.");
                return "bookForm";
            }

            // Find existing book location for the existing book.
            final BookData existingData = bookMapper.getBookDataForBook(existingBook.getId(), principal.getName());
            // if found, update the existing count and delete the old book location.
            if(existingData != null) {
                final int grandTotal = existingData.getTotal() + bookForm.getAmount();
                bookMapper.deleteBookLocation(id, principal.getName());
                bookMapper.updateBookLocation(existingData.getBookLocationId(), existingData.getBookId(),
                        grandTotal, principal.getName());

                return "redirect:/books";
            }

            bookMapper.addBookToActiveLocation(existingBook.getId(), bookForm.getAmount(), principal.getName());
        }

        return "redirect:/books";
    }

}
