package net.paksao.bookdepository.signup;

import net.paksao.bookdepository.appUser.AppUserMapper;
import net.paksao.bookdepository.domain.AppUser;
import net.paksao.bookdepository.location.Location;
import net.paksao.bookdepository.location.LocationMapper;
import org.apache.ibatis.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping("/signup")
public class SignupController {

    private static final Logger log = LoggerFactory.getLogger(SignupController.class);

    @Autowired
    private PasswordEncoder pwEncoder;

    @Autowired
    private AppUserMapper appUserMapper;

    @Autowired
    private LocationMapper locMapper;

    @GetMapping
    public String signup(final SignUpCredentials credentials) {
        return "signup";
    }

    private static final String REGISTRATION_MAX_ENV_VAR = "REGISTRATION_MAX";

    private int getMaxUserCount() {
        try {
            return Integer.parseInt(System.getenv(REGISTRATION_MAX_ENV_VAR));
        } catch(NumberFormatException ex) {
            return 1;
        }
    }

    @PostMapping
    @Transactional
    public String signup(@Valid final SignUpCredentials credentials, final BindingResult bindingResult,
                         final Model model, final RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "signup";
        }

        if (appUserMapper.appUserCount() >= getMaxUserCount()) {
            model.addAttribute("errorMessage", "No more users can be registered at this time.");
            return "signup";
        }

        if (appUserMapper.findByUsername(credentials.getUsername()) != null) {
            model.addAttribute("errorMessage", "Username has already been taken. Please select another.");
            return "signup";
        }

        final String encPassword = pwEncoder.encode(credentials.getPassword());

        final AppUser appUser = new AppUser();
        appUser.setName(credentials.getName());
        appUser.setPassword(encPassword);
        appUser.setUsername(credentials.getUsername());

        try {
            appUserMapper.registerUser(appUser);

            final var loc = new Location();
            loc.setName(appUser.getUsername());
            loc.setUserId(appUser.getId());

            locMapper.createDefaultLocation(loc);
            appUserMapper.setActiveLocation(appUser, loc, appUser.getUsername());
        } catch (final Exception ex) {
            log.error("Error registering user", ex);
            model.addAttribute("errorMessage", "An error has occurred, please try again later.");
            return "signup";
        }

        redirectAttributes.addFlashAttribute("message", "User created, you may now log in.");

        return "redirect:/login";
    }
}
