package net.paksao.bookdepository.signup;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class SignUpCredentials {

    @NotEmpty
    private String username;
    @NotEmpty
    @Size(min = 8, message = "must be at least 8 characters.")
    private String password;
    @NotEmpty
    private String name;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
