package net.paksao.bookdepository.book;

import net.paksao.bookdepository.domain.Book;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

@Ignore
public class OpenLibraryIsbnSearchIntTest {

    @Test
    public void testConnection() throws IOException {
       final Book book = OpenLibraryIsbnSearch.searchIsbn("9780448054704");

       assertEquals("9780448054704", book.getIsbn());
       assertEquals("Wizard Of Oz Cl (Com Lib)", book.getTitle());
       assertEquals("L. Frank Baum", book.getAuthor());
    }
}
