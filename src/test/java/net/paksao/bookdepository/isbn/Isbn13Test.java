package net.paksao.bookdepository.isbn;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class Isbn13Test {

    @Test
    public void checkFormat() {
        // Invalid ISBNs will return as null
        assertNotNull(Isbn13.code("9780448054704"));
        assertNotNull(Isbn13.code("9780395797259"));
        assertNotNull(Isbn13.code("978-0062498533 "));

        assertNull(Isbn13.code("9780448054705"));
        assertNull(Isbn13.code("9780485054704"));

        assertNull(Isbn13.code("0485054704"));
        assertNull(Isbn13.code("04850r4704"));
        assertNull(Isbn13.code("abcdefghijklm"));
        assertNull(Isbn13.code(""));
        assertNull(Isbn13.code(null));
    }
}
