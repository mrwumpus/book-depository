package net.paksao.bookdepository.isbn;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class Isbn10Test {

    @Test
    public void checkFormat() {
        assertNotNull(Isbn10.code("0-306-40615-2"));

        assertNull(Isbn10.code("1234567890"));
        assertNull(Isbn10.code("abcdefghij"));
        assertNull(Isbn10.code("123"));
        assertNull(Isbn10.code("12345g0026"));
        assertNull(Isbn10.code(""));
        assertNull(Isbn10.code(null));
    }
}
